from floodsystem.stationdata import build_station_list

def run():
    """
    print number of rivers having at least one monitoring station and 
    print the first 10 of these rivers in alphabetical order and
    print the names of the stations located on "River Aire", "River Cam", "Thames" in alphabetical order
    """

    #First task
    from floodsystem.geo import rivers_with_station

    stations = build_station_list() #build list of stations    
    
    rivers = rivers_with_station(stations)

    print("Number of rivers having at least one station:")       
    print(len(rivers)) #print number of rivers having at least one station
    
    print("----------")

    print("First 10 River Names:")
    print(rivers[:10]) #print the first 10

    print("----------")



    #Second Task
    from floodsystem.geo import stations_by_river

    stations = build_station_list() #build list of stations 

    river_wanted = stations_by_river(stations) #set a variable

    print("River Aire:")
    print(river_wanted["River Aire"])

    print("----------")

    print("River Cam:")
    print(river_wanted["River Cam"])

    print("----------")

    print("Thames:")
    print(river_wanted["Thames"])

    
if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    
    run()
