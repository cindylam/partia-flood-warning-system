"""Unit test for the analysis module"""

from floodsystem.analysis import polyfit
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
import random
import datetime

def test_polyfit():
    """Tests to see if the polyfit function is called"""
    stations = build_station_list() #build a list of station type MonitoringStation
    
    random_int = random.randint( 1 , len(stations)) #generate a random integer
    #within the range of the number of stations
    
    dt = random.randint(1 , 20) #generate a random number of days between 1 and 20
    
    p = random.randint(1 , 6) #generate a random number between 1 and 6
    #as the polynomial degree
    
    #find the dates and the levels for some random 
    dates , levels = fetch_measure_levels(stations[random_int].measure_id, dt=datetime.timedelta(days=dt))
    
    poly , shift = polyfit(dates , levels , p) 
    
    #tests to see if poly is the correct type
    assert type(poly) == np.lib.polynomial.poly1d 
    
    #test to see if shift is the correct type
    assert type(shift) == datetime.datetime