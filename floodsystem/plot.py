import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import matplotlib

import numpy as np



def plot_water_levels(station , dates , levels):
    """
    Takes the argument:
        1. station (Type MonitoringStation)
        2. dates (Type list of type datetime)
        3. levels (Type list of floats )
        
    And outputs a graph showing the dates against the water levels
    
    """
    
    t = dates #change variable dates to t
    
    level = levels #change variable levels to level
    
    if len(t) != len(level): #check if the lengths of either tuple is the same
        raise ValueError("the length of dates must be the same as the length of levels")
        
    typical_range = station.typical_range #extract typical range data from 
    #MonitoringStation object
    
    typical_low = [] #create empty list for later entry

    typical_high = [] #create empty list for later entry
    

    #this operation creates a list of the same values len(t) times
    #since the typical low and typical high is constant, we just want a list
    #of the same y values for plotting    
    for i in range(len(t)):
        
        typical_low_entry = [typical_range[0]]
        
        typical_low = typical_low + typical_low_entry
        
        
        typical_high_entry = [typical_range[1]]

        typical_high = typical_high + typical_high_entry
        
    #typical low and typical high is now a list of floats off all the same 
    #value
    
    typical_range_low = typical_range[0] #extract typical low data
    typical_range_high = typical_range[1] #extract typical high data
    
    print("Typical low is {}".format(typical_range_low)) #print the typical low
    print("Typical high is {}".format(typical_range_high)) #print the typical high
        

    plt.plot(t , typical_high) #plotting the straight lines of typical high
    
    plt.plot(t , typical_low) #plot the straight lines of typical low
    
    plt.plot(t , level) #plot the levels vs time
        
    
    plt.xlabel('date') #label x axis
    plt.ylabel('water level / m') #label y axis
    plt.xticks(rotation=45); #rotate dates to look nicer
    plt.title("{}".format(station.name)) #give the graph a title
    
    plt.tight_layout() #makes sure plot does not cut off date labels
    plt.show() #display plot
    
    

def plot_water_level_with_fit(station, dates , levels , p):
    """Takes the arguments:
            1. station (Type MonitoringClass)
            2. dates (Type: list of datetimes)
            3. levels (Type: list of floats)
        Plots the water level with best fit line polynomial of degree p
        
    """
    
    # Create set of 10 data points on interval (1000, 1002)
    x = matplotlib.dates.date2num(dates)
    y = levels
    
    typical_range = station.typical_range
    
    # Using shifted x values, find coefficient of best-fit
    # polynomial f(x) of degree p
    p_coeff = np.polyfit(x - x[0], y, p)
    
    # Convert coefficient into a polynomial that can be evaluated
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)
    
    num_of_points = len(y)
    typical_range_low = typical_range[0]
    typical_range_high = typical_range[1]

    typical_range_low = [typical_range_low]*num_of_points
    typical_range_high = [typical_range_high]*num_of_points

    plt.plot(x + x[0] , typical_range_low)
    plt.plot(x + x[0] , typical_range_high)
    
    plt.xlabel("dates as shifted floats")
    plt.ylabel("water_level / m")
    plt.title(station.name)
    
    # Plot original data points
    plt.plot(x + x[0], y, '.')
    
    # Plot polynomial fit at 30 points along interval (note that polynomial
    # is evaluated using the shift x)
    x1 = np.linspace(x[0], x[-1], 30)
        

    plt.plot(x1 + x[0], poly(x1 - x[0]))
    
    # Display plot
    plt.show()

    
    