import numpy as np
import matplotlib
import matplotlib.pyplot as plt


from datetime import datetime, timedelta
import datetime


def polyfit(dates , levels , p):
    """
    Takes the argument:
        1. dates (Type: datetime)
        2. levels (Type: list of water levels which are floats)
        3. p (Type: int) The degree of the polynomial
        
    Returns:
        1. polynomial object
        2. shift of date (time) axis
        
    """
    
    x = matplotlib.dates.date2num(dates) #convert dates to floats
    
    y = levels
    
    # Using shifted x values, find coefficient of best-fit
    # polynomial f(x) of degree p
    p_coeff = np.polyfit(x - x[0], y, p)
    
    # Convert coefficient into a polynomial that can be evaluated
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)
    
    shift_as_date = matplotlib.dates.num2date(x[0]) #convert the 64bit np integer
    #to a date
    
    
    return poly , shift_as_date