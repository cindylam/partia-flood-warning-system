
#For Task 2B
from .station import MonitoringStation
from .stationdata import update_water_levels
from operator import itemgetter


def stations_level_over_threshold(stations, tol):
    
    update_water_levels(stations) #update latest water level
    
    station_over_level = [] #empty list for stations having relative water level over tolerance
    
    for station in stations:
        
        if station.relative_water_level() != None and station.relative_water_level() > tol: 
        #kick out inconsistent stations and stations lower than tolerance
        
            station_over_level.append((station, station.relative_water_level()))
            #append the list with tuple of name and relative water level
           
    sorted_station_over_level = sorted(station_over_level , key=itemgetter(1), reverse=True)
    #sort by the second item in tuple(relative water level) and then reverse
           
    return sorted_station_over_level
    #return the sorted list
    
#For Task 2C
from .station import MonitoringStation
from .stationdata import update_water_levels
from operator import itemgetter

def stations_highest_rel_level(stations, N):
    
    update_water_levels(stations) #update latest water level
    
    station_level_list = [] #empty list for stations and their relative water level

    for station in stations:
        
        if station.relative_water_level() != None:
        #kick out inconsistent stations
        
            station_level_list.append((station, station.relative_water_level()))
            #append the list with tuple of name and relative water level
           
    sorted_station_level_list = sorted(station_level_list , key=itemgetter(1), reverse=True)
    #sort by the second item in tuple(relative water level) and then reverse
    
    final_station_level_list = sorted_station_level_list[:N]
       
    return final_station_level_list
    #return the sorted list