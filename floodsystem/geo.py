"""This module contains a collection of functions related to
geographical data.

"""


#Task 1B

from haversine import haversine

from .utils import sorted_by_key

def stations_by_distance(stations , p):
    """
    Takes the argument: 
        1. stations (type: MonitoringStation) and 
        2. coordinates (type: tuple)
    Returns:
        1. A list of tuples (station , distance) ordered by distance
            -distance (type Float)
    
    """

    
    stations_distance = [] #create empty list

    for station in stations:
        
        coord = station.coord #coord is the coordinate of the particular station
        
        distance = haversine(coord , p) #distance of station to p
        
        currentTuple = [(station,distance)] #create tuple containing the station
        #(type MonitoringStation) and distance to p (type Float)

        stations_distance = stations_distance + currentTuple #add currentTuple 
        #to overall list stations_distance
        
    stations_distance = sorted_by_key(stations_distance , 1) #sort the list by 
    #the second element in each tuple
    
    return stations_distance
    

    
#Task 1C
    
def stations_within_radius(stations , centre , r):
    """
    Takes the arguments:
        1. stations (type MonitoringStation)
        2. centre, coordinate (type Tuple)
        3. r, radius (type float)
    
    Returns:
        1. A list of stations within radius r of a geographic coordinate 'centre'
        in alphabetical order
    """
    
    
    
    #stations_distance is a list of ordered stations
    stations_distance = stations_by_distance(stations , centre)
    
    stations_inside_r = [] #create empty list eventually to contain a list of stations
    #inside radius r
    
    for station in stations_distance:
        
        if station[1] < r: #if station is within radius of coordinate centre
        #execute the following
            
            station_name = station[0].name
            
            currentTuple = [(station_name , station[1])] #create a tuple (inside a list) of the 
            #station name within radius r of centre coordinate
            
            stations_inside_r = currentTuple + stations_inside_r #add currentTuple
            #to existing list of station_inside_r
            
        else: #if station is more than (or equal to) radius from centre 
        #coordinate, pass (do nothing)
            pass

    
    stations_inside_r = sorted_by_key(stations_inside_r , 0) #sort the list station_in_r
    #by the first element
    
    return stations_inside_r
      
    
      
#Task 1D

def rivers_with_station(stations):
    """
    Takes the argument: 
        1. stations (type: MonitoringStation) 
    Returns:
        1. A set of all rivers (by name) with a monitoring station
    """

    
    
    river_name = [] #empty list for river names

    for station in stations:

        if station.river in river_name: #if names already appeared once, continue to the next dat
            continue
        else:
            river_name.append(station.river) #if not, add names to the list
 
    river_name.sort()

    return river_name  


def stations_by_river(stations):
    """
    Takes the argument: 
        1. stations (type: MonitoringStation) 
    Returns:
        1. A dictionary that maps river names (the ‘key’) to a list of stations on a given river
    """
    
    
    
    #first produce a list of river names
    
    river_name = [] #empty list for river names

    for station in stations:

        if station.river in river_name: #if names already appeared once, continue to the next dat
            continue
        else:
            river_name.append(station.river) #if not, add names to the list
 
    river_name.sort()
    
    #then build up the dictionary
    
    river_dict = {} #empty dictionary for mapping river with stations
    
    for i in range (len(river_name)): #iterate all river names
            
        station_of_river = [] #empty list for stations in same river at the start of each iteration
           
        for station in stations:
            
            if station.river in [river_name[i]]: #for stations in same river...
                
                station_of_river.append(station.name) #append to the list
                
            station_of_river.sort() #sort the list
            river_dict[river_name[i]] = station_of_river #append whole list to the dictionary
            

    return river_dict #return the dictionary      
    
    
    
#Task 1E

def rivers_by_station_number(stations, N):
    """
    Takes the argument: 
        1. stations (type: MonitoringStation) 
        2. N (type: float/int)
    Returns:
        1. a list of (river name, number of stations) tuples, sorted by the number of stations. 
        (In the case that there are more rivers with the same number of stations as the N th entry, 
        include these rivers in the list.)
    """
    
    
    from operator import itemgetter
    
    
    river_name = [] #empty list for river names
    

    for station in stations:
            
        if station.river in river_name: #if names already appeared once, continue to the next dat
            continue
        else:
            river_name.append(station.river) #if not, add names to the list
     
    river_name.sort()
        
    #then build up the list
    station_no_in_river = [] #empty list for the required tuples
    
    for i in range (len(river_name)): #iterate all river names
            
        n = 0 #set number of stations equals zero at the start of each iteration
           
        for station in stations:
            
            if station.river in [river_name[i]]: #for stations in same river...
                
                 n += 1 #increase n by 1 each time
                 
        station_no_in_river.append((river_name[i] , n))
        
    #need to sort by n, and then reverse the whole list to have greatest number as the first entry
    station_no_sorted = sorted(station_no_in_river , key=itemgetter(1), reverse=True) 
    
    #then need to reduce the list to only hold -- the first N items + the items having same smallest n   
    if station_no_sorted[N-1][1] != station_no_sorted[N][1]:
        final_n = N

    elif station_no_sorted[N-1][1] == station_no_sorted[N][1]:
        final_n = N
        for i in range (final_n, len(station_no_sorted)):
            if station_no_sorted[i-1][1] == station_no_sorted[i][1]:
                final_n += 1
            elif station_no_sorted[i-1][1] != station_no_sorted[i][1]:
                break
            
    station_no_sorted_reduced = station_no_sorted[:(final_n)]
    
    return station_no_sorted_reduced #return the list of tuples