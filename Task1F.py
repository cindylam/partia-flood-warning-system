from floodsystem.stationdata import build_station_list

from floodsystem.station import inconsistent_typical_range_stations

def run():
    """
    Print a list of stations names, in alphabetical order, for stations with inconsistent data
    """
    
    stations = build_station_list() #build the list of stations
    
    inconsistent_stations = inconsistent_typical_range_stations(stations)    

    inconsistent_stations_list = sorted(inconsistent_stations)
    
    print(inconsistent_stations_list)
    
if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")

    # Run Task1F
    run()
