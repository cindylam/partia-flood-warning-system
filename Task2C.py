
from floodsystem.stationdata import build_station_list, update_water_levels

from floodsystem.flood import stations_highest_rel_level


def run():
    """
    prints the names of the 10 stations at which the 
    current relative level is highest, with the relative level 
    beside each station name.
    """
    
    stations = build_station_list() #list of stations

    list_with_N_highest_station = stations_highest_rel_level(stations, N=10)
    #put in variables
    
    for n in range (len(list_with_N_highest_station)):
        print(list_with_N_highest_station[n][0].name, list_with_N_highest_station[n][1])
    #print the tuples one by one with items seperated

if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")
    
    run()     
