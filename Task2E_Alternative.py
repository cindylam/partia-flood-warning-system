#Task 2E Alternative

import datetime
from floodsystem.stationdata import build_station_list , update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels

def run():

    # Build list of stations
    stations = build_station_list()
    
    dt = 10 #set number of dates
        

    for station in stations[103:108]: #rather than the top 5 stations with highest level
    #use some other stations
        
        #call fetch_measure_levels in datafetcher.py for dates and levels
        dates , levels = fetch_measure_levels(station.measure_id,
                                         dt=datetime.timedelta(days=dt))

        #call the function plot_water_levels in plot.py to plot graph
        plot_water_levels( station , dates , levels)
        
    
        
        
        



if __name__ == "__main__":
    print("*** Task 2E Alternative: CUED Part IA Flood Warning System ***")

    run()