from floodsystem.stationdata import build_station_list

from floodsystem.geo import rivers_by_station_number   

 
def run():
    """
    prints the list of (number stations, river) tuples when N = 9
    """
    
    
    stations = build_station_list()
    number_of_station = rivers_by_station_number(stations, N=9)

    print(number_of_station)
    
    

if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")
    
    run()    
