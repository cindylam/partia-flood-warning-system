from floodsystem.geo import stations_by_distance

from floodsystem.stationdata import build_station_list

def run():
    """
    Prints a list of tuples (station name, town, distance) for the 10 
    closest and the 10 furthest stations from the Cambridge city centre, 
    (52.2053, 0.1218)
    """
    
    stations_list = build_station_list() #build a list of stations (type MonitoringStation)
    
    p = (52.2053 , 0.1218) #coordinate of Cambridge Station
    
    #list of tuples (station , distance) where station is an object type MonitoringStation
    #call function in module geo.py
    station_distance_raw = stations_by_distance(stations_list , p) 
    
    stations_distance = [] #create empty list 

    for station in station_distance_raw:
        
        station_name = station[0].name #extract the station name from station (MonitoringStation)
        
        station_town = station[0].town #extract the station town from station (MonitoringStation)
        
        station_dist = station[1] #extract the second element of the station tuple

        #build up the list of stations_distance
        currentTuple = [(station_name , station_town , station_dist)]

        stations_distance = stations_distance + currentTuple #add the currentTuple to the overall
        #list of stations
        
    first_10 = stations_distance[:10] #extracts the first 10 stations from stations_distance

    last_10 = stations_distance[-10:] #extracts the second 10 stations from stations_distance

    print(first_10)
    
    print(last_10)
    
if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")

    # Run Task1B
    run()

    
        