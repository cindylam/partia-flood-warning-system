from floodsystem.geo import stations_within_radius

from floodsystem.geo import stations_by_distance

from floodsystem.stationdata import build_station_list

def run():
    """
    Prints a list of names in alphabetical order for a list of stations within 
    10km of the Cambridge station
    """
    
    stations_list = build_station_list() #build the list of stations
    
    radius = 10 #set radius to 10km
    
    x = (52.2053 , 0.1218) #coordinate x which is the coordinate of Cambridge Station
    
    #build a list of stations within radius r by calling stations_within_radius 
    #function in module geo.py
    stations_inside_r = stations_within_radius(stations = stations_list , centre = x, r = radius)
    
    stations_name_inside_r =[] #create empty list to store only the names
    
    #only store the names in the list stations_inside_r
    for tuple in stations_inside_r:
        stations_name_inside_r = stations_name_inside_r + [tuple[0]]

    print(stations_name_inside_r)
    
if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")

    # Run Task1C
    run()