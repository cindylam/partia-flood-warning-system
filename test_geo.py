import pytest

from floodsystem.stationdata import build_station_list


#Test for Task 1B

from floodsystem.geo import stations_by_distance

def test_stations_by_distance():
    """
    Tests to see whether the stations are ordered
    """
    
    station_list = build_station_list() #call for the list of stations
    
    p = (52.2053 , 0.1218) #cambridge station coordinate
    
    station_distance = stations_by_distance(station_list , p) #call the list of stations
    
    for i in range(1,len(station_distance)-1): #the -1 because we don't want to 
    #compare the first value to the last value in the list
        
        assert station_distance[i][1] >= station_distance[i-1][1] #checks to
        #see whether the next item in ths list is indeed larger (or equal to)
        #the current item

        
        
#Test for Task 1C

import random

from floodsystem.geo import stations_within_radius

def test_stations_within_radius():
    """
    Tests to see whether the radius of the station is within a set radius 
    The set radius is a randomly generated float between 10 and 300
    """
    
    stations_list = build_station_list() #build up a list of MonitoringStation objects
    
    x = (52.2053 , 0.1218) #reference coordinate (Cambridge station)
    
    radius = random.uniform(10 , 300) #set the radius to some random float between 10 and 300
    
    #build a list of tuples of (station name, distance to centre)
    stations_inside_radius = stations_within_radius(stations = stations_list , centre = x , r = radius)
    
    for i in range(len(stations_inside_radius)):
    
        assert stations_inside_radius[i][1] < radius #check if the radius is less than
        #the randomly generated radius earlier
        
        
#Test for Task 1D

from floodsystem.geo import rivers_with_station

def test_rivers_with_station():
    """
    Test to see if there are names repeated
    """
    
    stations = build_station_list() #build the list
    
    all_river_list = rivers_with_station(stations)
    
    for i in range(1, len(all_river_list)):
        
        assert all_river_list[i-1] != all_river_list[i]
        #check if adjacent names in the sorted list is different
        
    
    
   
    
from floodsystem.geo import rivers_with_station   
from floodsystem.geo import stations_by_river
import random

def test_stations_by_river():
    """
    Test to see if the stations are under the correct 'key' of corresponding river name
    """
    
    stations = build_station_list()
    
    river_list = rivers_with_station(stations)
    
    river_station_dict = stations_by_river(stations)
    
    river_chosen = river_list[random.randint(0,len(river_station_dict)-1)]
    
    for i in range(0,len(river_station_dict[river_chosen])-1):
  
        station_chosen = river_station_dict[river_chosen][i]

        assert station_chosen.river == river_chosen
        #check that the stations belongs to that river
    
    

    
#Test for Task 1E

from floodsystem.geo import rivers_by_station_number
import random


def test_rivers_by_station_number():
    """
    Test to see if the list of tuple is sorted correctly
    in descending order of the number of stations in the river
    """
    
    stations = build_station_list() #build the list of station
    
    stations_needed = rivers_by_station_number(stations, N=random.randint(0, 100))
    #give an output list of random N
    
    for i in range(1, len(stations_needed)):
        
        assert stations_needed[i-1][1] >= stations_needed[i][1]
        #check if the number of station of the i-th river is larger than the (i-1)-th river
        
















    
    
    
    