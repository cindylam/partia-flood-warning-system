import numpy as np
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.analysis import polyfit
from floodsystem.flood import stations_level_over_threshold
import datetime

def run():

    stations = build_station_list() #build the station list
    
    #to reduce computing time, use only stations with relative water level
    #of over 0.7, since if we include all stations, the computing time will be very long
    stations_with_relative_level = stations_level_over_threshold(stations, tol = 0.7)
    
    stations = [] #create an empty list to store stations with rel water level over
    #3.7
    
    #append the stations with rel water level of over 0.7 to the empty list stations
    for tuple_entry in stations_with_relative_level:
    
        stations.append(tuple_entry[0])
    
    
    #create an empty list to store tuples of (stations (Type MonitoringStation , rise = True or False )
    station_rise = []
    
    
    dt = 5 #set the number of days to go back and graph
    p=4 #set the polynomial degree
    
    #this for loop appends to the empty list station_rise
    for station in stations:
        
        #find the dates and levels for the respective stations
        dates , levels = fetch_measure_levels(station.measure_id, 
                                         dt=datetime.timedelta(days=dt))
        
        
        #find the polynomial approximation to the water level graph
        poly , d0 = polyfit(dates , levels , p)
        
        #find the differential of the polynomial poly
        poly_der = np.polyder(poly) 
        
        #find the initial gradient of poly. the value is zero based on the definition
        #that poly is derived based on the latest date, and all other dates are negative
        #relative to the latest date. Therefore, the latest date is zero
        grad = poly_der(0)
        
        #assign whether rise is true or false
        if grad < 0: #if the water level is falling, rise = False
            rise = False
            
        elif grad > 0: #if the water level is rising, rise = True
            rise = True
        else: #if it so happens that the grad == 0, then assign None to rise
            rise = None
            
        #extract the relative water level info from station
        rel_level = station.relative_water_level()
        
        #create tuple containing (station Type MonitoringStation , rel_level Type float
        #and whether the water level is rising or not)
        station_grad_entry = (station , rel_level , rise)
        
        #append the entry to the overall empty list
        station_rise.append(station_grad_entry)
        
    
    town_risk = [] #create an empty list to store tuples of (Town Type string , risk = 
    #severe , high , moderate or low)
    
    #appends to the empty list town_risk
    for tuple_entry in station_rise:   
        
        #tuple entry has three parts, give names to those three parts
        station = tuple_entry[0]
    
        rel_level = tuple_entry[1]
    
        rise = tuple_entry[2]
    
    
        #assign whether each town has a severe, high, moderate or low risk of flooding
        if rise == True:
            if rel_level >= 1:
                risk = "severe"
            elif rel_level >=0.8:
                risk = "high"
            elif rel_level >=0.7:
                risk = "moderate"
            else:
                risk = "low"
        else:
            if rel_level >= 1:
                risk = "high"
            if rel_level >= 0.8:
                risk = "moderate"
            else:
                risk = "low"
        #add station town name and risk level to the tuple
        town_risk_entry = (station.town , risk)
        
        #append the tuple with the flood risk information to the overall
        #empty list
        town_risk.append(town_risk_entry)
        
    
    #create empty lists to store towns of different risks
    towns_severe = []
    towns_high = []
    towns_moderate = []
    towns_low = []
    
    #append to the empty lists
    for tuple_entry in town_risk:
        if tuple_entry[1] == "severe":#if severe append to towns_severe
            towns_severe.append(tuple_entry[0])
        elif tuple_entry[1] == "high":#if high append to towns_high
            towns_high.append(tuple_entry[0])
        elif tuple_entry[1] == "moderate":#if moderate append to towns_moderate
            towns_moderate.append(tuple_entry[0])
        else:
            towns_low.append(tuple_entry[0])#if low append to towns_low
            
                             
    #print the flood warning for different towns        
    print("*** ATTENTION: List of Towns with Flood Warnings ***")
    
    print("---Towns with SEVERE risk of flood---")
    
    for town in towns_severe:
        print(town)
        
    
    print("")
    print("---Towns with HIGH risk of flood---")
    
    for town in towns_high:
        print(town)
    
    print("")
    print("---Towns with MODERATE risk of flood---")
    
    for town in towns_moderate:
        print(town)
    
    print("")
    print("---Towns with LOW risk of flood---")
    
    for town in towns_low:
        print(town)

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    
    run()    