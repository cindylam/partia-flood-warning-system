
from floodsystem.stationdata import build_station_list, update_water_levels

from floodsystem.flood import stations_level_over_threshold   

 
def run():
    """
    prints the name of each station at which 
    the current relative level is over 0.8, 
    with the relative level alongside the name
    """
    
    stations = build_station_list() #list of stations

    list_station_over_level = stations_level_over_threshold(stations, tol = 0.8)
    #put in variables
    
    for n in range (len(list_station_over_level)):
        print(list_station_over_level[n][0].name, list_station_over_level[n][1])
    #print the tuples one by one with items seperated

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")
    
    run()    