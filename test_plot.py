"""Unit test for the plot module"""

import datetime

from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
import random

from floodsystem.plot import plot_water_levels
from floodsystem.plot import plot_water_level_with_fit

def test_plot_water_levels():
    """Tests to see if the plot_water_levels function is called"""
    stations = build_station_list() #build a list of station type MonitoringStation
    
    random_int = random.randint( 1 , len(stations)) #generate a random integer
    #within the range of the number of stations
    
    dt = random.randint(1 , 20) #generate a random number of days between 1 and 20
    
    
    #find the dates and the levels for some random 
    dates , levels = fetch_measure_levels(stations[random_int].measure_id, dt=datetime.timedelta(days=dt))
    
    x = plot_water_levels(stations[random_int] , dates , levels)
    
def test_plot_water_level_with_fit():
    """Tests to see if the plot_water_level_with_fit function is called"""
    
    stations = build_station_list() #build a list of station type MonitoringStation
    
    random_int = random.randint( 1 , len(stations)) #generate a random integer
    #within the range of the number of stations
    
    dt = random.randint(1 , 20) #generate a random number of days between 1 and 20
    
    p = random.randint(1 , 6)
    
    #find the dates and the levels for some random 
    dates , levels = fetch_measure_levels(stations[random_int].measure_id, dt=datetime.timedelta(days=dt))
    
    #call the function plot_water
    x = plot_water_level_with_fit(stations[random_int] , dates , levels , p)