
from floodsystem.stationdata import build_station_list

#For Task 2B
from floodsystem.flood import stations_level_over_threshold
import random

def test_stations_level_over_threshold():
    """
    Test to see if the relative water levels for stations in the list 
    are really higher than tolerance
    """
    stations = build_station_list() #build station list
    
    random_tolerance = random.uniform(0,1) #set a random number
    
    test_list = stations_level_over_threshold(stations, tol=random_tolerance)
    #build a list with stations relative water level higher than the random tolerance
    
    for i in range (len(test_list)):
        
        assert test_list[i][1] > random_tolerance
        #ensure the item in the list have relative level higher than tolerance


#For Task 2C
from floodsystem.flood import stations_highest_rel_level
import random

def test_stations_highest_rel_level():
    """
    Test to see if the tuples are sorted correctly
    """
    
    stations = build_station_list() #build station list
    
    random_N = random.randint(0,100)
    #generate a random number
    
    test_list = stations_highest_rel_level(stations, N=random_N)
    #build the required list with random N terms
    
    for i in range (1, len(test_list)):
        
        assert test_list[i-1][1] > test_list[i][1]
        #ensure they are sorted by descending order of relative level
