#Task 2F alternative

import datetime
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.flood import stations_highest_rel_level


def run():

    # Build list of stations
    stations = build_station_list()
    
    dt = 10 #set number of dates
    
    N=5 #set number of stations
    
    p = 4 #polynomial degree    
     

    for station in stations[103:108]:
        
        #call fetch_measure_levels in datafetcher.py for dates and levels    
        dates , levels = fetch_measure_levels(station.measure_id,
                                         dt=datetime.timedelta(days=dt))

        #call the function plot_water_levels in plot.py to plot graph
        plot_water_level_with_fit(station, dates , levels , p)

if __name__ == "__main__":
    print("*** Task 2F Alternative: CUED Part IA Flood Warning System ***")

    run()

